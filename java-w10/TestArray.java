import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class TestArray {
	
	public static void main(String[] args) throws FileNotFoundException {
		ArrayList<String> test = new ArrayList<String>();
		try
		{
			Scanner plik = new Scanner(new File("./"+args[0]));
			while(plik.hasNext())
			{
				test.add(plik.next());
			}
			
			plik.close();
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Nie ma takiego pliku: " + args[0]);
			System.exit(-1);
		}
		
		System.out.println(test);
		test.sort(null);
		System.out.println(test);


	}

}
