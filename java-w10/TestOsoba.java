import java.time.LocalDate;
import java.util.ArrayList;


import pl.imiajd.razniewski.*;

public class TestOsoba {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		ArrayList<Osoba> grupa = new ArrayList<Osoba>();
		grupa.add(new Osoba("Baran", LocalDate.of(1994, 10, 10)));
		grupa.add(new Osoba("Maklewicz", LocalDate.of(1994, 10, 10)));
		grupa.add(new Osoba("Janas", LocalDate.of(1993, 10, 10)));
		grupa.add(new Osoba("Janas", LocalDate.of(1992, 9, 1)));
		grupa.add(new Osoba("Razniewski", LocalDate.of(1994, 11, 17)));
		
		System.out.println(grupa);
		grupa.sort(null);
		System.out.println(grupa);
		grupa.sort(Osoba.OsobaDateComparator);
		System.out.println(grupa);
		
			

	}

}
