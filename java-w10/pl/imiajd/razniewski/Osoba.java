package pl.imiajd.razniewski;

import java.time.LocalDate;
import java.util.Comparator;

public class Osoba implements Cloneable, Comparable<Osoba> {

	public Osoba(String nazwisko, LocalDate dataUrodzenia)
	{
		this.nazwisko = nazwisko;
		this.dataUrodzenia = dataUrodzenia;
	}
	
	public String toString()
	{
		
		return getClass().getSimpleName() + " " + "<" + nazwisko + " " + dataUrodzenia + ">";
		
	}
	
	public boolean equals(Object porownywany)
	{
		if(this == porownywany)
		{
			return true;
		}
		
		if(!( this.getClass().equals(porownywany.getClass()) ))
		{
			return false;
		}
		
		
		Osoba innaOsoba = (Osoba)porownywany;
		
		if((this.getNazwisko().equals(innaOsoba.getNazwisko())) && this.getDataUrodzenia().equals(innaOsoba.getDataUrodzenia()))
		{
			return true;
		}
		
		return false;
	}
		
	
	public int compareTo(Osoba o) 
	{
		
		if(this.getNazwisko().compareTo(o.getNazwisko()) > 0)
		{
			return 1;
		}
		
		else if(this.getNazwisko().compareTo(o.getNazwisko()) < 0)
		{
			return -1;
		}
		
		if(this.getDataUrodzenia().compareTo(o.getDataUrodzenia()) > 0)
		{
			return 1;
		}
		
		else if(this.getDataUrodzenia().compareTo(o.getDataUrodzenia()) < 0)
		{
			return -1;
		}
		
		return 0;
	}
	
	
	public static Comparator<Osoba> OsobaDateComparator = new Comparator<Osoba>()
	{
			public int compare(Osoba os1, Osoba os2)
			{
				LocalDate datU1 = os1.getDataUrodzenia();
				LocalDate datU2 = os2.getDataUrodzenia();
				
				return datU1.compareTo(datU2); 
			}
	};	
	
	public Osoba clone() throws CloneNotSupportedException
	{
		Osoba cloned = (Osoba)super.clone();
		
		
		return cloned;
		
	}

	public String getNazwisko()
	{
		return nazwisko;
	}
	
	public LocalDate getDataUrodzenia()
	{
		return dataUrodzenia;
	}
	
	private String nazwisko;
	private LocalDate dataUrodzenia;
	
}
