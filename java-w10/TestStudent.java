import java.time.LocalDate;
import java.util.ArrayList;


import pl.imiajd.razniewski.*;

public class TestStudent {

	/**
	 * @param args
	 * @throws CloneNotSupportedException 
	 */
	public static void main(String[] args) throws CloneNotSupportedException 
	{
		ArrayList<Student> grupa = new ArrayList<Student>();
		grupa.add(new Student("Baran", LocalDate.of(1994, 10, 10), 3.65));
		grupa.add(new Student("Maklewicz", LocalDate.of(1994, 10, 10), 3.55));
		grupa.add(new Student("Janas", LocalDate.of(1993, 10, 10), 4.4));
		grupa.add(new Student("Janas", LocalDate.of(1992, 9, 1), 2.4));
		grupa.add(new Student("Razniewski", LocalDate.of(1994, 11, 17), 4.55));
		
		
		
		System.out.println(grupa);
		grupa.sort(null);
		System.out.println(grupa);
		
		grupa.sort(Student.StudentSredniaComparator);
		System.out.println(grupa);
		
			

	}

}

