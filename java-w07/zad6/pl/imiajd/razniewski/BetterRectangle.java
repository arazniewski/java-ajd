package pl.imiajd.razniewski;

import java.awt.*;

public class BetterRectangle extends java.awt.Rectangle
{
    public BetterRectangle()
    {
        super();
    }
    
    
    public double getPerimeter()
    {
        return 2 * (super.getWidth() + super.getHeight());
    }
    
    public double getArea()
    {
       return super.getWidth() * super.getHeight(); 
    }
}
