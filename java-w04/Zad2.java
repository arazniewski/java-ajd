import java.util.Scanner;

public class Zad2
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Podaj ciag 1: ");
        String s = in.next();
        System.out.println("Podaj ciag 2: ");
        String k = in.next();

        System.out.println(countSubStr(s, k));

    }

    public static int countSubStr(String str, String str2)
    {
        int x = 0;
        int i = 0;
        while(i < (str.length() - str2.length()+1))
        {
            if(str.substring(i, i+str2.length()).equals(str2))
            {
                x++;
            }
            i++;
        }
        return x;
    }

}

