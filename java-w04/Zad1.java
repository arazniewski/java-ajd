import java.util.Scanner;

public class Zad1
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Podaj chara: ");
        char s = in.next().charAt(0);
        System.out.println("Podaj ciag: ");
        String k = in.next();

        System.out.println(countChar(k, s));

    }

    public static int countChar(String str, char c)
    {
        int x = 0;
        for(char z: str.toCharArray())
        {
            if(z == c)
            {
                x++;
            }
        }
        return x;
    }

}

