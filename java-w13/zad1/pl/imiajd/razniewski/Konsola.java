package pl.imiajd.razniewski;

import java.util.Scanner;
import java.util.*;

public class Konsola
{
	public Konsola()
	{
		kolejka = new PriorityQueue<Pair<String>>();
		System.out.println("dodaj priorytet opis - dodaje zadanie");
		System.out.println("nastepne - wykonuje zadanie");
		System.out.println("zakoncz - zakancza program");
		
		Scanner sluchacz = new Scanner(System.in);
		String polecenie = "";
		while(!polecenie.equals("zakoncz"))
		{
			polecenie = sluchacz.nextLine();

			if(!polecenie.equals("zakoncz"))
			{
				rozpoznajPolecenie(polecenie);
			}
		}
	}


	private void rozpoznajPolecenie(String polecenie)
	{
		if(polecenie.equals("nastepne"))
		{
			nastepne();
		}
		else
		{
			String[] temp = polecenie.split(" ");
			if(temp[0].equals("dodaj") && Integer.parseInt(temp[1]) >= 0 && !(temp[2].equals("")))
			{
				Integer temp1 = Integer.parseInt(temp[1]);
				String opistemp = "";
				for(int j = 2; j < temp.length; j++)
				{
					opistemp = opistemp + temp[j] + " ";
				}

				if(kolejka.offer(new Pair(temp1, opistemp)))
				{
					System.out.println("Dodano zadanie!");
				}


			}
		}

	}
	
	private void nastepne()
	{
		Pair<String> temp = kolejka.poll();
		if(temp != null)
		{
			System.out.println("Zadanie o priorytecie: " + temp.getP() + " i opisie: " + temp.getD() + " zostalo wykonane!");
		}
		else
		{
			System.out.println("Nie ma zadnych zadan do wykonania");
		}

	}

	private PriorityQueue<Pair<String>> kolejka;
}


class Pair<S> implements Comparable<Pair<S>>
{
	Pair(Integer pierwsze, S drugie)
	{
		this.pierwsze = pierwsze;
		this.drugie = drugie;
	}
	
	public Integer getP()
	{
		return pierwsze;
	}

	public S getD()
	{
		return drugie;
	}
	
	@Override
	public int compareTo(Pair<S> obj)
	{
		if(obj.getP().compareTo(this.getP()) > 0)
		{
			return -2;
		}
		else if(obj.getP().compareTo(this.getP()) < 0)
		{
			return 2;
		}

		return 0;
	}

	private Integer pierwsze;
	private S drugie;
}
