import java.util.*;

class Main
{
	public static void main(String[] args)
	{
		Structure mapa = new Structure();
		mapa.dodaj("Kowalski", "db");
		mapa.dodaj("Adamusor", "dst");
		mapa.dodaj("Palik", "bdb");
		mapa.dodaj("Debil", "db");
		mapa.zamien("Kura", "ndst");
		mapa.zamien("Debil", "ndst");
		mapa.wyswietl();
	}
}



class Structure 
{
	Structure()
	{
		mapa = new TreeMap<String, String>();
	}

	public void dodaj(String key, String value)
	{
		mapa.put(key, value);
	}

	public void usun(String key)
	{
		mapa.remove(key);
	}

	public void zamien(String student, String ocena)
	{
		if(mapa.containsKey(student))
		{
			mapa.put(student, ocena);
		}
	}
	public void wyswietl()
	{
		for(String s: mapa.keySet())
		{
			System.out.println(s + ": " + mapa.get(s));
		}
	}


	private Map<String, String> mapa;
}

