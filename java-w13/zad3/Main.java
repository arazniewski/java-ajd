import java.util.*;

class Main
{
	public static void main(String[] args)
	{
		Structure mapson = new Structure();
		mapson.dodaj("Kowalski", "db");
		mapson.dodaj("Adamusor", "dst");
		mapson.dodaj("Palik", "bdb");
		mapson.dodaj("Radziejewski", "prawilniak");
		mapson.dodaj("Debil", "db");
		mapson.zamien("Kura", "ndst");
		mapson.zamien("Debil", "ndst");
		mapson.wyswietl();
	}
}



class Structure 
{
	Structure()
	{
		mapa = new TreeMap<Student, int>();
	}

	public void dodaj(String key, String value)
	{
		mapa.put(key, value);
	}

	public void usun(String key)
	{
		mapa.remove(key);
	}

	public void zamien(String student, String ocena)
	{
		if(mapa.containsKey(student))
		{
			mapa.put(student, ocena);
		}
	}
	public void wyswietl()
	{
		for(String s: mapa.keySet())
		{
			System.out.println(s + ": " + mapa.get(s));
		}
	}


	private Map<String, String> mapa;
}

class Student implements Comparable<Student>
{
	Student(String imie, String nazwisko)
	{
		this.imie = imie;
		this.nazwisko = nazwisko;
	}

	public int compareTo(Student other)
	{
		if(other.nazwisko.equals(this.nazwisko) < 0)
		{
			return -1;
		}
		else if(other.nazwisko.equals(this.nazwisko) > 0)
		{
			return 1;
		}
		
		if(other.imie.equals(this.imie) < 0)
		{
			return -2;
		}
		else if(other.imie.equals(this.imie) > 0)
		{
			return 2;
		}
		
		return 0;
	}


	private String nazwisko;
	private String imie;

}
