import java.util.*;

class Main
{
	public static void main(String[] args)
	{
		String test = "co co co co test testowo bardzo barbara, asda, lalala, test";

		String[] tokenizowane = Utils.doTablicy(test);
		
		Map<String, Integer> mapka = Utils.doMapy(tokenizowane);
		
		for(String s: mapka.keySet())
		{
			System.out.println("Hash: " + Utils.getHash(s) + "\tslowo: " + s + "\tilosc:" + mapka.get(s));
		}
	}
}


class Utils
{
	public static String bezKropek(String s)
	{
		String wynik = "";

		for(char b: s.toCharArray())
		{
			if(b != '.' && b != ',')
			{
				wynik = wynik + b;
			}

		}

		return wynik.toLowerCase();
	}

	public static String[] doTablicy(String s)
	{
		StringTokenizer st = new StringTokenizer(Utils.bezKropek(s));
		String[] tokenizowane = new String[st.countTokens()];
		int i = 0;
		while(st.hasMoreTokens())
		{
			tokenizowane[i] = st.nextToken();
			i++;
		}

		return tokenizowane;
	}
	
	public static Map<String, Integer> doMapy(String[] tokenizowane)
	{
		Map<String, Integer> mapka = new HashMap<String, Integer>();
		for(int j = 0; j < tokenizowane.length; j++)
		{
			if(mapka.containsKey(tokenizowane[j]))
			{
				mapka.put(tokenizowane[j], mapka.get(tokenizowane[j])+1);
			}
			else
			{
				mapka.put(tokenizowane[j], 1);
			}
		}

		return mapka;

	}

	public static int getHash(String slowo)
	{
		return slowo.hashCode();
	}


}
