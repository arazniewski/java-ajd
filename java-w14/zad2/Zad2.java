import java.util.*;
import java.io.*;

class Zad2
{
	public static void main(String[] args)
	{
		try
		{
			DataInputStream in = new DataInputStream(new FileInputStream(args[0]));
			byte[] b = new byte[64];

			while(in.read(b) != -1)
			{
				for(int i = 0; i < 64; i++)
				{
					if(b[i] >= 32 && b[i] <= 126)
					{
						System.out.print(Character.toChars(b[i]));
					}
				}
				b = new byte[64];
				System.out.print('\n');
			}
		}
		catch (IOException ex)
		{
			System.out.println("Wystapil blad!");
		}
	}
}
