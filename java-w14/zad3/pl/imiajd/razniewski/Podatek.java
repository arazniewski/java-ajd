package pl.imiajd.razniewski;

import java.io.*;
import java.util.*;


public class Podatek
{
	public Podatek(){};
	public Podatek(String nazwa, double cena, int ilosc)
	{
		this.nazwa = nazwa;
		this.cena = cena;
		this.ilosc = ilosc;
	}


	public void writeData(DataOutput out)
	throws IOException
	{
		DataUtils.writeFixedString(nazwa, NAME_SIZE, out);
		out.writeDouble(cena);
		out.writeInt(ilosc);
	}

	public void readData(DataInput in)
	throws IOException
	{
		nazwa = DataUtils.readFixedString(NAME_SIZE, in);
		cena = in.readDouble();
		ilosc = in.readInt();
	}
	
	public String toString()
	{
		return getClass().getName() + " " + nazwa + " " + cena + " " + ilosc;
	}

	public String getNazwa()
	{
		return nazwa;
	}

	public double getCena()
	{
		return cena;
	}

	public int getIlosc()
	{
		return ilosc;
	}
	
	public static ArrayList<Podatek> wczytajPodatki(String sciezka)
	{
		ArrayList<Podatek> podatki = new ArrayList<Podatek>();

		try
		{
			RandomAccessFile in = new RandomAccessFile(sciezka, "r");

			int n = (int)(in.length() / Podatek.RECORD_SIZE);
			for(int s = n - 1; s >= 0; s--)
			{
				Podatek temp = new Podatek();
				in.seek(s * Podatek.RECORD_SIZE);
				temp.readData(in);

				podatki.add(temp);
			}

			in.close();
		}
		catch (IOException a)
		{
			System.out.println("BLAD!");
		}

		return podatki;
	}

	public static void zapiszPodatki(ArrayList<Podatek> tab, String sciezka)
	{
		try
		{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(sciezka));
	
			for(Podatek s: tab)
			{
				s.writeData(out);
			}
		
			out.close();
		}
		catch (IOException e)
		{
			System.out.println("BLAD!");
		}

	}

	public static Podatek szukaj(String sciezka, String nazwa)
	{
		Podatek temp = new Podatek();
		try
		{
			RandomAccessFile in = new RandomAccessFile(sciezka, "r");
			int n = (int)(in.length() / Podatek.RECORD_SIZE);
			String temp2 = "";
			for(int s = n - 1; s >= 0; s--)
			{
				in.seek(s * Podatek.RECORD_SIZE);
				if((temp2 = DataUtils.readFixedString(Podatek.NAME_SIZE, in)).equals(nazwa))
				{
					double sds = in.readDouble();
					int bsd = in.readInt();
					temp = new Podatek(temp2, sds, bsd);
				}
			}
		}
		catch(IOException e)
		{
			System.out.println("BLAD!");
			e.printStackTrace();
		}

		return temp;
	}

	public void setCena(double c)
	{
		this.cena = c;
	}

	public void setIlosc(int ilosc)
	{
		this.ilosc = ilosc;
	}






	private String nazwa;
	private double cena;
	private int ilosc;
	
	public static final int NAME_SIZE = 30;
	public static final int RECORD_SIZE = NAME_SIZE * 2 + 8 + 4;

}



class DataUtils
{

	public static String readFixedString(int size, DataInput in)
	throws IOException
	{
		StringBuilder b = new StringBuilder(size);
		int i = 0;
		boolean more = true;

		while(more && i < size)
		{
			char ch = in.readChar();
			i++;

			if(ch == 0)
			{
				more = false;
			}
			else
			{
				b.append(ch);
			}
		}

		in.skipBytes(2 * (size - i));
	
		return b.toString();
	}


	public static void writeFixedString(String s, int size, DataOutput out)
	throws IOException
	{
		for(int i = 0; i < size; i++)
		{
			char ch = 0;

			if(i < s.length())
			{
				ch = s.charAt(i);
			}

			out.writeChar(ch);
		}
	}
}
