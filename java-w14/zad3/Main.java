import pl.imiajd.razniewski.*;

import java.io.*;
import java.util.*;

class Main
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		ArrayList<Podatek> tab = new ArrayList<Podatek>();
		String b = "";
		while(!b.equals("zakoncz"))
		{
			System.out.println("Komendy: wczytaj, wypisz, zapisz, dodaj, szukajiwypisz, zamiencena, zamienilosc");
			b = in.nextLine();
			switch(b)
			{
				case "wczytaj": tab = Podatek.wczytajPodatki("podatki.dat");
								System.out.println("Wczytano!");
								break;
				case "wypisz": wypisz(tab);
							   break;
				case "zapisz": Podatek.zapiszPodatki(tab, "podatki.dat");
							   System.out.println("Zapisano");
							   break;
				case "dodaj": tab.add(dodajP(in));
							  break;
				case "szukajiwypisz": System.out.println("Podaj nazwe towaru");
							  System.out.println(Podatek.szukaj("podatki.dat", in.nextLine()));
									  break;
				case "zamiencena" : zamiencena(tab, in);
									break;
				case "zamienilosc" : zamienilosc(tab, in);
									 break;
									  

			}


		}
	}

	public static<K> void wypisz(ArrayList<K> x)
	{
		for(K b: x)
		{
			System.out.println(b);
		}
	}

	public static Podatek dodajP(Scanner in)
	{
		System.out.println("Podaj: nazwa, cena, ilosc");
		String nazwa = in.nextLine();
		double cena = in.nextDouble();
		int ilosc = in.nextInt();
		return new Podatek(nazwa, cena, ilosc);
	}

	public static void zamiencena(ArrayList<Podatek> tab, Scanner in)
	{
		System.out.println("Podaj nazwe i nowa cene");
		String nazwa = in.nextLine();
		double cena = in.nextDouble();
		for(Podatek s: tab)
		{
			if(s.getNazwa().equals(nazwa))
			{
				s.setCena(cena);
			}
		}
	}
	public static void zamienilosc(ArrayList<Podatek> tab, Scanner in)
	{
		System.out.println("Podaj nazwe i nowa ilosc");
		String nazwa = in.nextLine();
		int ilosc = in.nextInt();
		for(Podatek s: tab)
		{
			if(s.getNazwa().equals(nazwa))
			{
				s.setIlosc(ilosc);
			}
		}
	}
}
