package pl.szyfrator;
import java.util.*;
import java.io.*;

public class Szyfrator
{
	public Szyfrator(String[] args)
	{
		literki = new HashMap<String, String>();
		literki2 = new HashMap<String, String>();
		
		
		if(Arrays.asList(args).contains("-d"))
		{
			czy = true;
			klucz = args[2];
			odS = args[3];
			doS = args[4];
		}
		else
		{
			czy = false;
			klucz = args[1];
			odS = args[2];
			doS = args[3];
		}
		System.out.println(klucznik());
		ustawKlucz();
		try
		{
			zapiszdo();
		} catch(IOException e)
		{
			System.out.println("CHUJ");
		}
	}
	

	private void ustawKlucz()
	{
		String ss = klucznik();
		int b = 0;
		for(char s: ss.toCharArray())
		{
	
			literki.put(l[b], ((Character)s).toString());
			literki2.put(((Character)s).toString(), l[b]);
			b++;
		}
		int bss = 0;
		for(int g = b; g < l2.length; g++)
		{
			while(literki.containsValue(l2[bss]))
			{
				bss++;
			}
			literki.put(l[g], l2[bss]);
			literki2.put(l2[bss], l[g]);
			bss++;
		}





	}

	private void zapiszdo() throws IOException
	{
		BufferedReader in = new BufferedReader(new FileReader(odS));
		PrintWriter out = new PrintWriter(new FileWriter(doS));
		
		String linia = "";

		while((linia = in.readLine()) != null)
		{
			if(czy)
			{
				linia = deszyfruj(linia);
			}
			else
			{
				linia = szyfruj(linia);
			}

			out.println(linia);
		}

		in.close();
		out.close();



	}



	private String klucznik()
	{
		ArrayList<String> temp = new ArrayList<String>();
		for(Character s: klucz.toCharArray())
		{
			if(!temp.contains(s.toString().toUpperCase()))
			{
				temp.add(s.toString().toUpperCase());
			}
		}

		String glowny = "";

		for(String s: temp)
		{
			glowny = glowny + s;
		}

		return glowny.toUpperCase();
	}
	public boolean isSzyfrowany()
	{
		return czy;
	}

	public String getKlucz()
	{
		return klucz;
	}

	public String getOd()
	{
		return odS;
	}

	public String getDo()
	{
		return doS;
	}
	
	public String szyfruj(String zdanie)
	{
		String wynik = "";
		for(char s: zdanie.toCharArray())
		{
			String temp = ((Character)s).toString().toUpperCase();
			if(literki.containsKey(temp))
			{
				wynik = wynik + literki.get(temp);
			}
			else
			{
				wynik = wynik + temp;
			}
		}

		return wynik;
	}

	public String deszyfruj(String zdanie)
	{
		String wynik = "";
		for(char s: zdanie.toCharArray())
		{
			String temp = ((Character)s).toString().toUpperCase();
			if(literki2.containsKey(temp))
			{
				wynik = wynik + literki2.get(temp);
			}
			else
			{
				wynik = wynik + temp;
			}
		}

		return wynik;
	}

	private boolean czy;
	private String klucz;
	private String odS;
	private String doS;
	private Map<String, String> literki;
	private Map<String, String> literki2;
	private String[] l = {"A", "Ą", "B", "C", "Ć", "D", "E", "Ę", "F", "G", "H", "I", "J", "K", "L", "Ł", "M", "N", "Ń", "O", "Ó", "P", "R", "S", "Ś", "T", "U", "W", "Y", "Z", "Ź", "Ż"};
	private String[] l2 = {"Ż", "Ź", "Z", "Y", "W", "U", "T", "Ś", "S", "R", "P", "Ó", "O", "Ń", "N", "M", "Ł", "L", "K", "J", "I", "H", "G", "F", "Ę", "E", "D", "Ć", "C", "B", "Ą", "A"};
}
