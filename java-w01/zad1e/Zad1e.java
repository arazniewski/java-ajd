import java.util.Scanner;
import java.lang.Math.*;

public class Zad1e {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 1e");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        int rozwiazanie = 1;
        int liczba;
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            rozwiazanie = rozwiazanie * Math.abs(liczba);
        }
        System.out.println("Twoj wynik to: " + rozwiazanie);
        
    }

}

