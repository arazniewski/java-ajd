import java.util.Scanner;

public class Zad2a {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 2a");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        int rozwiazanie = 0;
        int liczba;
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            if(liczba%2 == 1)
            {
                rozwiazanie = rozwiazanie + 1;
            }
        }
        System.out.println("Twoj wynik to: " + rozwiazanie);
        
    }

}

