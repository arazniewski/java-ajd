import java.util.Scanner;

public class Zad12 {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 12");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        int liczba = 0;
        int stara = 0;
        String ciag = "";
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            if(i == 0)
            {
                stara = liczba;
            }
            else
            {
                ciag = ciag + " " + Integer.toString(liczba);
            }
        }
        System.out.println(ciag + " " + stara);
        
    }

}

