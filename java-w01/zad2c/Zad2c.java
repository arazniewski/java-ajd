import java.util.Scanner;

public class Zad2c {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 2c");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        int rozwiazanie = 0;
        int liczba;
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            if((liczba%3 == 0) && (liczba%5 != 0))
            {
                rozwiazanie = rozwiazanie + 1;
            }
        }
        System.out.println("Twoj wynik to: " + rozwiazanie);
        
    }

}

