import java.util.Scanner;
import java.lang.Math.*;

public class Zad1f {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 1f");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        double rozwiazanie = 0;
        double liczba;
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            rozwiazanie = rozwiazanie + Math.pow(liczba, 2);
        }
        System.out.println("Twoj wynik to: " + rozwiazanie);
        
    }

}

