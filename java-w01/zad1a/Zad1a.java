import java.util.Scanner;

public class Zad1a {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 1a");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        int suma = 0;
        int liczba;
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            suma = suma + liczba;
        }
        System.out.println("Twoj wynik to: " + suma);
        
    }

}

