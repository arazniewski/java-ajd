import java.util.Scanner;

public class Zad1b {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 1b");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        int rozwiazanie = 1;
        int liczba;
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            rozwiazanie = rozwiazanie * liczba;
        }
        System.out.println("Twoj wynik to: " + rozwiazanie);
        
    }

}

