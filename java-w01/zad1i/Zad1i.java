import java.util.Scanner;
import java.lang.Math.*;

public class Zad1i {
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);

        System.out.println("Zadanie 1i");

        System.out.println("Podaj ile liczb wpiszesz: ");
        int n = in.nextInt();
        double rozwiazaniedod = 0;
        double liczba;
        for(int i = 0;i < n; i++)
        {
            System.out.println("Podaj liczbe: ");
            liczba = in.nextInt();
            liczba = (liczba * Math.pow(-1, i+1))/silnia(i+1);
            rozwiazaniedod = rozwiazaniedod + liczba;
        }
        System.out.println("Twoj wynik dodawania to: " + rozwiazaniedod);
        
    }

    private static int silnia(int n) 
    {
        int silnia = 1;
        for (int i = 1; i <= n; i++) {
            silnia = silnia * i;
        }
        return silnia;
    }
}
