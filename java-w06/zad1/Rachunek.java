import java.util.*;

public class Rachunek
{
    public static void main(String[] args)
    {
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);

        RachunekBankowy.setRocznaStopaProcentowa((0.04));
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        
        saver1.wyswietl("saver1");
        saver2.wyswietl("saver2");

        RachunekBankowy.setRocznaStopaProcentowa((0.05));
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        
        saver1.wyswietl("saver1");
        saver2.wyswietl("saver2");

    }
}


class RachunekBankowy
{
    private double saldo;
    static double rocznaStopaProcentowa = 0;


    public RachunekBankowy(double saldo)
    {
        this.saldo = saldo;
    }

    public void obliczMiesieczneOdsetki()
    {
        double temp = (saldo * rocznaStopaProcentowa)/12;
        this.saldo = this.saldo + temp;
    }

    public static void setRocznaStopaProcentowa(double x)
    {
        rocznaStopaProcentowa = x;
    }

    public void wyswietl(String nazwa)
    {
        System.out.println("Saldo na koncie: " + nazwa + " " + this.saldo + " | Stopa: " + rocznaStopaProcentowa);
    }
}
