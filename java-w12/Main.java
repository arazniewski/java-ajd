import java.util.LinkedList;
import java.util.Iterator;
import java.util.Stack;
import java.util.ArrayList;
import java.util.Scanner;
import java.lang.*;


class Main
{
	public static void main(String[] args)
	{
		LinkedList<String> llist = new LinkedList<String>();

		llist.add("Raz");
		llist.add("Dwa");
		llist.add("Trzy");

		llist.add("Raz");
		llist.add("Dwa");
		llist.add("Trzy");

		llist.add("Raz");
		llist.add("Dwa");
		llist.add("Trzy");

		llist.add("Raz");
		llist.add("Dwa");
		llist.add("Trzy");

		Utils.redukuj(llist, 2);

		System.out.println(llist);
		
		Utils.odwroc(llist);
		System.out.println(llist);
		System.out.println("Raz dwa trzy. Mam nadzieje ze dobrze.");
		Utils.przetworzString("Raz dwa trzy. Mam nadzieje ze dobrze.");
		Scanner in = new Scanner(System.in);

		System.out.print("Podaj liczbe ktora podziele: ");
		int s = in.nextInt();
		Utils.cyfry(s);
		System.out.print("Podaj liczbe, a wytworze do niej sito erystotanesa: ");
		int n = in.nextInt();
		Utils.erato(n);

		Utils.print(llist);
	}
}



class Utils
{
	public static <T> void redukuj(LinkedList<T> llist, int n)
	{
		int dz = 0;
		Iterator<T> iter = llist.iterator();
		while(iter.hasNext())
		{
			iter.next();
			dz++;
			if(dz % n == 0)
			{
				iter.remove();
			}
		}
	}

	public static <T> void odwroc(LinkedList<T> llist)
	{

		Iterator<T> iter = llist.iterator();
		LinkedList<T> temp = new LinkedList<T>();

		while(iter.hasNext())
		{
			T s = iter.next();
			temp.offerFirst(s);
			iter.remove();
		}

		llist.addAll(temp);
	
	}

	public static void przetworzString(String zdanie)
	{
		ArrayList<Stack<String>> llist = new ArrayList<Stack<String>>();
		String skladowa = "";
		Stack<String> temp = new Stack<String>();
		for(char s: zdanie.toCharArray())
		{
			if(s == '.')
			{
				temp.push(skladowa.toLowerCase());
				llist.add(temp);
				skladowa = "";
				temp = new Stack<String>();
			}
			else if(s == ' ')
			{
				if(!skladowa.equals(""))
				{
					temp.push(skladowa.toLowerCase());
				}
				skladowa = "";
			}
			else
			{
				skladowa = skladowa + s;
			}
		}

		boolean oc = true;
		for(int j = 0; j < llist.size(); j++)
		{
			while(!llist.get(j).empty())
			{

				String tempS = llist.get(j).pop();
				if(oc)
				{
					char zam = tempS.charAt(0);
					char zam1 = Character.toUpperCase(zam);
					tempS = tempS.replace(zam, zam1);
				}

				System.out.print(tempS);
				if(!llist.get(j).empty())
				{
					System.out.print(" ");
				}

				oc = false;
			}
			oc = true;
			System.out.print(". ");
		}
		System.out.println("");
	
	}

	public static void cyfry(int x)
	{
		Stack<Integer> stos = new Stack<Integer>();
		int cyfra;
		int liczba = x;
		int licznik = 0;
		
		do
		{
			cyfra = x%10;
			stos.push(cyfra);
			x = x / 10;
			licznik++;
		} while(x != 0);

		while(licznik != 0)
		{
			licznik--;
			System.out.print(stos.pop() + " ");
		}

		System.out.println("");
	}

	public static void erato(int n)
	{
		ArrayList<Integer> s = new ArrayList<Integer>();

		for(int j = 2; j <= n; j++)
		{
			s.add(j);
		}
		System.out.println(s);
		double pierw = Math.sqrt(n);
		for(int j = 2; j <= pierw; j++)
		{
			for(int i = 0; i < s.size(); i++)
			{
				if(s.get(i) != j && (s.get(i) % j == 0))
				{
					s.remove(i);
				}
			}
		}

		System.out.println(s);
	}

	public static <S, T extends Iterable<S>> void print(T x)
	{
		Iterator iterator = x.iterator();
		while(iterator.hasNext())
		{
			System.out.print(iterator.next() + ", ");
		}

		System.out.println("");
	}


}
